import React, { useState } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import JoinRoom from "./components/JoinRoom/JoinRoom";
import Chat from "./components/Chat/Chat";

import LoginFetch from './login-fetch';
import axios from 'axios';

const httpClient = axios.create();
const loginFetch = new LoginFetch(httpClient);

//1. Router 경로 설정
const App = () => {
const [name, setName] = useState("");
const [password, setPassword] = useState("");

  return (
  <BrowserRouter>
    <Switch>
      <Route exact path="/">
        <JoinRoom name={name} password={password} setName={setName} setPassword={setPassword} loginFetch={loginFetch} />
      </Route>
      <Route path="/chat/">
        <Chat name={name} password={password} />
      </Route>
    </Switch>
  </BrowserRouter>
  )
};

export default App;