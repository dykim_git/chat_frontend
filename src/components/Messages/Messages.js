import React, { useRef, useEffect } from "react";

import Message from "./Message/Message";

import "./Messages.css";

const Messages = ({ messages, forwardedRef }) => {
  console.log('load messagessssss');
  return (
  <div className="messages" ref={forwardedRef}>
    {messages.map((message, i) => ( //(value, key, 본체)
      <div key={i}>
        <Message message={message} />
      </div>
    ))}
  </div>
)};

export default Messages;