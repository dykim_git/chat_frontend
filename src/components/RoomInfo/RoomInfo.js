import React from "react";
import "./RoomInfo.css";

import Button from "@material-ui/core/Button";

function RoomInfo() {
  return (
    <div className="roomInfo">
      <div className="leftInfo">
        <h3 className="leftInfo-text">{`Chatting Room`}</h3>
      </div>
      <div className="rightInfo">
        <Button
          className="infoButton"
          variant="raised"
          href="/"
          color="inherit"
        >
          close
        </Button>
      </div>
    </div>
  );
}

export default RoomInfo;
