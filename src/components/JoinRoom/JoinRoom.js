import React from "react";
import { useHistory } from 'react-router-dom';
import "./JoinRoom.css";

// Material-UI 적용하기 (입력 엘리먼트를 이미 예쁘게 디자인된 React 컴포넌트 형태로 제공)
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import template from "../../image/agilesoda.PNG";

const JoinRoom = ({ loginFetch, name, password, setName, setPassword }) => {
  const history = useHistory();

  const onLogin = (e) => {
    e.preventDefault();
    loginFetch.login(name, password)
      .then(json => {
        if (json.message === 'fail') {
          alert('아이디또는 비밀번호를 잘못입력하셨습니다.')
        } else {
          history.push({
            pathname: './chat',
          });
        }
      })
      
  }

  return (
    <div className="joinOuterContainer">
      <div className="joinInnerContainer">
        <div className="joinImageContainer">
          <img
            className="joinImage"
            src={template}
          />
        </div>
        <div className="joinFormContainer" component={Paper}>
          <div className="joinFormBox">
            <div className="joinTitle">
              <Typography component="h1" variant="h5">
                Login
              </Typography>
            </div>
            <form className="joinForm" noValidate> {/*<form> 태그의 novalidate 속성은 form data를 서버로 제출할 때 해당 데이터의 유효성을 검사하지 않음을 명시. 기본 false*/}
              <TextField //내부적으로 Material UI의 다른 컴포넌트인 <InputLabel/>,<Input/> (단순히 사용자의 입력을 받는 엘리먼트),<FormHelperText/>등으로 구성돼있는 고수준 컴포넌트
                variant="outlined"
                margin="normal"
                required
                fullWidth //default가 false
                id="name"
                name="name"
                label="Name" //입력해야하는 내용이 무엇인지를 알려주는 엘리먼트
                autoComplete="name"
                autoFocus
                onChange={(e) => setName(e.target.value)}
                onKeyPress={(e) => (e.key === "Enter" ? onLogin(e) : null)}
              />
              <TextField
                variant="outlined"
                margin="normal"
                required //form data가 서버로 제출되기 전 꼭 채워져 있어야 하는 입력 필드를 명시
                fullWidth //default가 false. If true, the input will take up the full width of its container.
                id="password"
                name="password"
                label="password" //ui상
                type="password" //?
                autoComplete="current-password" //자동완성
                onChange={(e) => setPassword(e.target.value)}
                onKeyPress={(e) => (e.key === "Enter" ? onLogin(e) : null)}
              />
              <div className="buttonBox mt-10">
                  <Button
                    type="submit"
                    fullWidth 
                    variant="contained"
                    color="primary"
                    className="joinButton mt-20"
                    onClick={(e) => onLogin(e)}
                  >
                    Sign In
                  </Button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default JoinRoom;
