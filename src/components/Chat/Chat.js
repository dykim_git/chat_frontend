import React, { useRef, useEffect, useState } from "react";
import { useHistory } from 'react-router-dom';
import io from "socket.io-client";
import "./Chat.css";

// 하위 컴포넌트
import Messages from "../Messages/Messages";
import RoomInfo from "../RoomInfo/RoomInfo";
import Input from "../Input/Input";

// Material-ui
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";

let socket;

const Chat = ({ name, password }) => {
  console.log('load chat');
  const scrollBox = useRef(null);

  const history = useHistory();
  const [message, setMessage] = useState("");
  const [messages, setMessages] = useState([]);

  useEffect(() => {
    socket = io(process.env.REACT_APP_SERVER_ADDR); // 소켓 연결
    socket.emit("join", name);
  }, []);

  const scroll = () => {
    const { scrollHeight, clientHeight } = scrollBox.current;
    scrollBox.current.scrollTop = scrollHeight - clientHeight;
  };

  useEffect(() => {
    // 서버에서 message 이벤트가 올 경우에 대해서 `on`
    socket.on("initMessage", (reciveMessage) => {
      const parseMessage = reciveMessage.map((m) => {
        return {
            name: name,
            sender: m.sender,
            text: m.text,
        };
      });
      setMessages([...messages, ...parseMessage]);
      scroll();
    });
  }, [messages]);

  useEffect(() => {
    // 서버에서 message 이벤트가 올 경우에 대해서 `on`
    socket.on("message", (reciveMessage) => {
      if (reciveMessage.sender == "error") {
        alert(reciveMessage.text);
        history.push({
          pathname: '/',
        });
      };

      setMessages([...messages, 
        {
          name: name,
          sender: reciveMessage.sender,
          text: reciveMessage.text,
        }
      ]);
    });
  }, [messages]);

  // 메세지 보내기 함수
  const sendMessage = (e) => {
    e.preventDefault();
    if (message) {
      socket.emit("sendMessage", { sender: name, text: message });
      setMessage("");
    }
  };

  return (
    <div className="chatOuterContainer">
      <div className="chatInnerContainer">
        <div className="appbar">
          <AppBar color="primary">
            <Toolbar className="toolBar">
              <Typography variant="h4" color="inherit" noWrap>
                AgileChat
              </Typography>
              <Button color="inherit" href="/">
                close
              </Button>
            </Toolbar>
          </AppBar>
        </div>
        <div className="chatScreen">
          <Paper elevation={5} className="chatScreenPaper"> 
            <RoomInfo />
            <Messages messages={messages} name={name} forwardedRef={scrollBox}/>
            <Input
              message={message}
              setMessage={setMessage}
              sendMessage={sendMessage}
            />
          </Paper>
        </div>
      </div>
    </div>
  );
};

export default Chat;