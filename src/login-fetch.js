
class LoginFetch {
  async login(name, password) {
    const response = await fetch(process.env.REACT_APP_SERVER_ADDR, {
      method: 'POST', // *GET, POST, PUT, DELETE, etc.
      mode: 'cors', // no-cors, cors, *same-origin
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, *same-origin, omit
      headers: {
          'Content-Type': 'application/json',
          // 'Content-Type': 'application/x-www-form-urlencoded',
      },
      redirect: 'follow', // manual, *follow, error
      referrer: 'no-referrer', // no-referrer, *client
      body: JSON.stringify({ name: name, password: password}), // body data type must match "Content-Type" header
  })
    const result = await response.json();
    return result;
  }
}

export default LoginFetch;